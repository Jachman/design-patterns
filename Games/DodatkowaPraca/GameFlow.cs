﻿using System;
using GameLibrary;
using Infrastructure;

namespace DodatkowaPraca
{
    internal class GameFlow
    {
        private readonly IScoreManager _scoreManager;
        private readonly IGamesProvider _gamesProvider;
        private bool _isOver = false;
        private bool _isWon = false;

        public GameFlow(IGamesProvider gamesProvider, IScoreManager scoreManager)
        {
            _gamesProvider = gamesProvider;
            _scoreManager = scoreManager;
        //    _scoreManager.GamesProvider = gamesProvider;
            _gamesProvider.Messages2 += Write;
            _gamesProvider.GameFinished2 += Write2;
        }

        public void Run()
        {
            Console.Clear();
            Console.WriteLine("Podaj swoje imie:");
            var name = Console.ReadLine();


            Console.WriteLine("Wybierz grę:");
            foreach (var gameName in _gamesProvider.GetGameNames())
            {
                Console.WriteLine($"- {gameName}");
            }

            _gamesProvider.ChooseGame(Console.ReadLine());

            bool result;
            do
            {
                Console.Write("Zgadnij: ");
                var bet = Console.ReadLine();
                
                result = _gamesProvider.Guess(bet);
                if (_isOver)
                    break;
            } while (!result);
            /////////////////////////
            if (_isWon)
            {
                var highscoreEntry = new HighscoreEntry(name, _gamesProvider.Result);
                _scoreManager.Add(highscoreEntry);
            }

            Console.WriteLine("\nNAJLEPSZE WYNIKI:");
            foreach (var entry in _scoreManager.List())
            {
                Console.WriteLine(entry);
            }
            ////////////////////////
        }

        public void Write(object oSender, MessagesEventArgs oMessagesEventArgs)
        {
            Console.WriteLine(oMessagesEventArgs.Result);
        }
        public void Write2(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        {
            _isOver = true;
            if (oGameFinishedEventArgs.IsWon)
                _isWon = true;
            Console.WriteLine(oGameFinishedEventArgs.IsWon
                ? $"Wygrales - {oGameFinishedEventArgs.Result} punktow."
                : $"Przegrales");
        }
    }
}
