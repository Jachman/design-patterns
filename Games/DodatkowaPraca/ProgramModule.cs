﻿using Ninject.Modules;
using Infrastructure;
using GameLibrary;

namespace DodatkowaPraca
{
    class ProgramModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IGamesProvider>().To<GamesProvider>().InSingletonScope();
            Bind<IScoreManager>().ToProvider(new ScoreManagerProvider());

            Bind<IGame>().To<GuessGame>();
            Bind<IGame>().To<MastermindGame>();
            Bind<IGame>().To<ReflexGame>();
            Bind<IGame>().To<TimeBasedGuessGame>();
            Bind<IGame>().To<TimeBasedMastermindGame>();
        }
    }
}
