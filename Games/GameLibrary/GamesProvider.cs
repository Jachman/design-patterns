﻿using System;
using System.Linq;
using Infrastructure;

namespace GameLibrary
{
    public class GamesProvider : IGamesProvider
    {
        private readonly IGame[] _games;
        private IGame _chosenGame;
        public event EventHandler<MessagesEventArgs> Messages2;
        public event EventHandler<GameFinishedEventArgs> GameFinished2;

        public GamesProvider(IGame[] games)
        {
            this._games = games;
        }

        public string[] GetGameNames()
        {
            return _games.Select(g => g.Name).ToArray();
        }

        public void ChooseGame(int n)
        {
            _chosenGame = _games[n];
        }

        public void ChooseGame(string name)
        {
            _chosenGame = _games.First(g => g.Name == name);
            _chosenGame.Messages += InitiateNextEvent;
            _chosenGame.GameFinished += InitiateNextEvent2;
        }

        public bool Guess(string bet)
        {
            return _chosenGame.Guess(bet);
        }

        public int Result
        {
            get { return _chosenGame.Result; }
        }

        public void InitiateNextEvent(object oSender, MessagesEventArgs oMessagesEventArgs)
        {
            Messages2?.Invoke(this, oMessagesEventArgs);
        }

        public void InitiateNextEvent2(object oSender, GameFinishedEventArgs oGameFinishedEventArgs)
        {
            GameFinished2?.Invoke(this, oGameFinishedEventArgs);
        }
    }
}
