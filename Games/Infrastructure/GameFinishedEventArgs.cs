﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class GameFinishedEventArgs
    {
        public GameFinishedEventArgs(bool isWon, int result)
        {
           IsWon  = isWon ;
           Result = result;
        }

        public bool IsWon { get; private set; }

        public int Result { get; private set; }
    }
}
