using System;

namespace Infrastructure
{
    public interface IGamesProvider
    {
        event EventHandler<MessagesEventArgs> Messages2;
        event EventHandler<GameFinishedEventArgs> GameFinished2;
        string[] GetGameNames();
        void ChooseGame(int n);
        void ChooseGame(string name);
        void InitiateNextEvent(object oSender, MessagesEventArgs oEventArgs);
        bool Guess(string bet);
        int Result { get; }
    }
}