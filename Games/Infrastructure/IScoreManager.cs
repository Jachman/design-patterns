namespace Infrastructure
{
    public interface IScoreManager
    {
        HighscoreEntry[] List();
        void Add(HighscoreEntry entry);
        //void GameOver(object oSender, GameFinishedEventArgs oGameFinishedEventArgs);
        string Name { get; set; }
        //IGamesProvider GamesProvider { get; set; }
    }
}